﻿using System;
using System.Security.AccessControl;
using CsEncryption;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsEncryptionTests
{
    [TestClass]
    public class PasswordAesTests
    {
        /// <summary>
        /// Tests validity of password
        /// </summary>
        [TestMethod]
        public void PasswordAesTest()
        {
            string wrong = null;
            PasswordAes aes;
            try
            {
                aes = new PasswordAes(wrong);
                Assert.Fail();
            }
            catch (ArgumentNullException)
            {
                //ignored
            }

            string right = "42";
            try
            {
                aes = new PasswordAes(right);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// Checks encryption/decryption
        /// </summary>
        [TestMethod]
        public void EncryptTest()
        {
            const string toEncrypt = "Why so serious?";
            const string password = "42";
            byte[] cipher;

            using (var aes = new PasswordAes(password))
                cipher = aes.Encrypt(toEncrypt);

            try
            {
                string plain;
                using (var aes = new PasswordAes(password))
                    plain = aes.Decrypt(cipher);
                Assert.AreEqual(toEncrypt, plain);
            }
            catch (IncorrectPasswordException)
            {
                Assert.Fail("bad password");
            }
        }
    }
}