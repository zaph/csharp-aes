﻿using System;
using CsEncryption;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsEncryptionTests
{
    [TestClass]
    public class AdvancedAesTests
    {
        /// <summary>
        /// Tests for valid and invalid key sizes
        /// </summary>
        [TestMethod]
        public void KeySizeTest()
        {
            #region invalid

            byte[][] invalidKeys =
            {
                new byte[125],
                new byte[64],
                new byte[512],
            };
            // ReSharper disable once NotAccessedVariable
            AdvancedAes aes;

            foreach (var invalidKey in invalidKeys)
            {
                try
                {
                    aes = new AdvancedAes(invalidKey);
                    Assert.Fail();
                }
                catch (ArgumentException)
                {

                }
            }

            #endregion

            #region valid

            byte[] validKey = new byte[256 / 8];

            try
            {
                aes = new AdvancedAes(validKey);
            }
            catch
            {
                Assert.Fail();
            }

            #endregion
        }

        /// <summary>
        /// Test string encryption
        /// </summary>
        [TestMethod]
        public void EncryptTest()
        {
            const string toEncrypt = "Why so serious";
            byte[] key=new byte[256/8];

            byte[] cipher;
            using (var aes = new AdvancedAes(key))
                cipher = aes.Encrypt(toEncrypt);

            using (var aes = new AdvancedAes(key))
                Assert.AreEqual(toEncrypt, aes.Decrypt(cipher));
        }

        /// <summary>
        /// Tests dummy class encryption
        /// </summary>
        [TestMethod]
        public void EncryptObjectTest()
        {
            DummyUser u = new DummyUser { Name = "Jon Snow" };

            using (var aes = new AdvancedAes())
            {
                byte[] cipher = aes.Encrypt(u);

                Assert.AreEqual(u.Name, aes.Decrypt<DummyUser>(cipher).Name);
            }
        }

        /// <summary>
        /// Tests for wrong keys
        /// </summary>
        [ExpectedException(typeof(IncorrectPasswordException))]
        [TestMethod]
        public void WrongKeyTest()
        {
            const string toEncrypt = "The cake is a lie";
            byte[] key1 = new byte[256 / 8],
                key2 = new byte[256 / 8];

            key2[0] = 1;

            byte[] cipher;
            using (var aes = new AdvancedAes(key1))
                cipher = aes.Encrypt(toEncrypt);

            using (var aes = new AdvancedAes(key2))
            {
                string decrypted = aes.Decrypt(cipher);
                Assert.Fail();
            }
        }
    }
}