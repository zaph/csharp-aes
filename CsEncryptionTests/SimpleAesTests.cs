﻿using System;
using CsEncryption;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsEncryptionTests
{
    [TestClass]
    public class SimpleAesTests
    {
        /// <summary>
        /// Tests for valid and invalid key sizes
        /// </summary>
        [TestMethod]
        public void KeySizeTest()
        {
            #region invalid

            byte[][] invalidKeys =
            {
                new byte[125],
                new byte[64],
                new byte[512],
            };
            // ReSharper disable once NotAccessedVariable
            SimpleAes aes;

            foreach (var invalidKey in invalidKeys)
            {
                try
                {
                    aes = new SimpleAes(invalidKey);
                    Assert.Fail();
                }
                catch (ArgumentException)
                {

                }
            }

            #endregion

            #region valid

            byte[][] validKeys =
            {
                new byte[128/8],
                new byte[192/8],
                new byte[256/8],
            };

            foreach (var validKey in validKeys)
            {
                try
                {
                    aes=new SimpleAes(validKey);
                }
                catch
                {
                    Assert.Fail();
                }
            }
            #endregion
        }

        /// <summary>
        /// Tests encryption / decryption
        /// </summary>
        [TestMethod]
        public void EncryptDecryptTest()
        {
            using (SimpleAes aes = new SimpleAes())
            {
                const string toDecrypt = "Gravity is a myth, Earth sucks";

                byte[] cipher = aes.Encrypt(toDecrypt);

                Assert.AreEqual(toDecrypt,aes.Decrypt(cipher));
            }
        }
    }
}