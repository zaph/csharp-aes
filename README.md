![csharp-aes](https://gitlab.com/uploads/project/avatar/1458372/lock.png "csharp-aes")

# csharp-aes

AES encryption for C# using 256-bit Rijndael algorithm.
## Samples

### Encrypt a string using a random key

```csharp
string input = "Hello world",
       password = "42",
       plainText;
byte[] cipher, key, iv;

//encrypt
using (SimpleAes aes = new SimpleAes())
{
    cipher = aes.Encrypt(input);
    key = aes.Key;//encryption key
    iv = aes.IV;//initialization vector
}

//decrypt
using (SimpleAes aes = new SimpleAes(key, iv))
    plainText = aes.Decrypt(cipher);

Console.WriteLine(plainText);
//output: Hello world
```
### Encrypt an object using a string password
First, our class:
```csharp
public class Person
{
    public string Name { get; set; }

    [XmlIgnore]
    public int Age { get; set; }
}
```
then the code:
```csharp
Person toEncrypt = new Person { Name = "Lenka", Age = 18 },
       decrypted;
string password = "42";

byte[] cipher;

using (Password256Aes aes = new Password256Aes(password))
{
    cipher = aes.Encrypt(toEncrypt);

    decrypted = aes.Decrypt<Person>(cipher);
}

Console.WriteLine($"{decrypted.Name} is {decrypted.Age} yrs old");
//output: Lenka is 0 yrs old
```

XmlSerializer is used to serialize objects, which means that when `XmlIgnore` was declared on `Person.Age` it was not saved. You may tweak the class this way.

*Note that both `SimpleAes` and `Password256Aes` are supposed to be used in `using` blocks*

## Installation

### Option 1- Package manager

Open Package Manager Console and type:
```
PM> Install-Package csharp-aes
```
### Option 2-Cloning repository

Download this repository
```Bash
git clone https://gitlab.com/czubehead/csharp-aes.git
```
In Visual Studio solution explorer right-click on your solution, select `add` -> `existing project`. Select the path to cloned repo.
Also you want to use it, so you need to add a reference- in solution explorer, right-click your project select `add`-> `Reference...` . On the left select `project` and `CsEncryption` should appear in the list. Check it and click OK.