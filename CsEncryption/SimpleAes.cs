﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace CsEncryption
{
    /// <summary>
    ///     Simple wrapper around <see cref="RijndaelManaged" /> class. Uses 256-bit
    ///     keys.
    /// </summary>
    public class SimpleAes : IDisposable
    {
        /// <summary>
        ///     Gets the length of
        ///     <see cref="SymmetricAlgorithm.IV" /> .
        ///     Bytes of this length are prepended to every encrypted string and
        ///     contain iv itself
        /// </summary>
        /// <remarks>
        ///     Length of arrray, not bits
        /// </remarks>
        public const int IVLength = BlockSize/8;

        /// <summary>
        /// Gets the default key size
        /// </summary>
        public const int DefaultKeySize = 256;

        /// <summary>
        ///     Gets the min length to be deciphered
        /// </summary>
        /// <remarks>
        ///     Length of the array, not in bits
        /// </remarks>
        public const int MinimumCipherLength = BlockSize/8;

        /// <summary>
        ///     Gets the valid key sizes in bits.
        /// </summary>
        public static readonly int[] ValidKeySizes = {128, 192, 256};

        private readonly RijndaelManaged _rijndael;

        /// <summary>
        ///     New <see cref="SimpleAes" /> with known <paramref name="key" /> and
        ///     <see cref="IV" />
        /// </summary>
        /// <param name="key"><see cref="Key" /> used to encrypt/decrypt</param>
        /// <param name="iv">
        ///     <see cref="IV" /> used to encrypt/decrypt. Use <see langword="null" />
        ///     to generate random
        /// </param>
        /// <exception cref="ArgumentException">Thrown when <paramref name="key"/> or <see cref="iv"/> are of invalid size</exception>
        /// /// <exception cref="ArgumentNullException">Thrown when <paramref name="key"/> is null</exception>
        public SimpleAes(byte[] key, byte[] iv = null)
        {
            _rijndael = DefaultRijndaelInstance();
            Key = key;
            if (iv == null)
                _rijndael.GenerateIV();
            else
                IV = iv;
        }

        /// <summary>
        ///     New <see cref="SimpleAes" /> with random
        ///     <see cref="CsEncryption.SimpleAes.Key" /> and
        ///     <see cref="CsEncryption.SimpleAes.IV" />
        /// </summary>
        public SimpleAes()
        {
            _rijndael = DefaultRijndaelInstance();

            _rijndael.GenerateIV();
            _rijndael.GenerateKey();
        }

        /// <summary>
        ///     Gets or sets the key in use.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when <paramref name="value"/> is of invalid size</exception>
        /// /// <exception cref="ArgumentNullException">Trown when <paramref name="value"/> is null</exception>
        public byte[] Key
        {
            get { return _rijndael?.Key; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));
                if (!ValidateKeySize(value))
                    throw new ArgumentException("Key length is invalid!");
                _rijndael.Key = value;
            }
        }

        /// <summary>
        ///     Gets or sets the initialization vector in use
        /// </summary>
        /// <remarks>
        /// The array length must be <see cref="IVLength"/> 
        /// </remarks>
        public byte[] IV
        {
            get { return _rijndael?.IV; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));
                if (value.Length != IVLength)
                    throw new ArgumentException("Invalid iv size");
                _rijndael.IV = value;
            }
        }

        /// <summary>
        ///     Gets the block size of <c>Rijndael</c> alg in bits
        /// </summary>
        public const int BlockSize = 128;

        #region IDisposable Members

        /// <summary>
        ///     <see cref="Dispose" />
        /// </summary>
        public void Dispose()
        {
            _rijndael?.Dispose();
        }

        #endregion

        /// <summary>
        ///     Encrypts string to bytes using 256bit <c>Rijndael</c> .
        /// </summary>
        /// <param name="plainText">Text to be encoded</param>
        /// <returns>
        ///     Cipher in bytes
        /// </returns>
        public byte[] Encrypt(string plainText)
        {
            if (plainText == null) throw new ArgumentNullException(nameof(plainText));

            var encryptor = _rijndael.CreateEncryptor();

            // Create the streams used for encryption.
            using (var msEncrypt = new MemoryStream())
            {
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (var swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(plainText);
                    }
                    //result of encryption
                    var dec = msEncrypt.ToArray();
                    return dec;
                }
            }
        }

        /// <summary>
        ///     <see cref="Decrypt" /> <paramref name="cipher" /> made by
        ///     <see cref="SimpleAes.Encrypt" /> with known
        ///     <see cref="CsEncryption.SimpleAes.IV" /> and
        ///     <see cref="CsEncryption.SimpleAes.Key" />
        /// </summary>
        /// <param name="cipher">Cipher to be decrypted.</param>
        /// <returns>
        ///     Decrypted string
        /// </returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="cipher"/> is null</exception>
        /// <exception cref="IncorrectPasswordException">Thrown when encryption key doesn't correspond to the correct one</exception>
        public string Decrypt(byte[] cipher)
        {
            if (cipher == null) throw new ArgumentNullException(nameof(cipher));

            // Create a decryptor to perform the stream transform.
            var decryptor = _rijndael.CreateDecryptor();

            // Create the streams used for decryption.
            try
            {
                using (var msDecrypt = new MemoryStream(cipher))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.

                            return srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            catch (CryptographicException)
            {
                throw new IncorrectPasswordException();
            }
        }

        /// <summary>
        ///     Validates the length size of <paramref name="key" />
        /// </summary>
        /// <param name="key">key to validate</param>
        /// <returns>
        ///     if <paramref name="key" /> is valid
        /// </returns>
        protected static bool ValidateKeySize(byte[] key)
            => ValidKeySizes.Contains(key?.Length*8 ?? -1);

        /// <summary>
        ///     Gets a default instance of <see cref="RijndaelManaged" /> with
        ///     explicitly set block and key sizes.
        /// </summary>
        /// <remarks>
        ///     Block size=128, key size=256
        /// </remarks>
        /// <returns>
        /// </returns>
        protected static RijndaelManaged DefaultRijndaelInstance()
            => new RijndaelManaged
            {
                BlockSize = BlockSize,
                KeySize = DefaultKeySize
            };
    }
}