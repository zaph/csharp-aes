﻿using System;
using System.Diagnostics;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace CsEncryption
{
    /// <summary>
    ///     Represents an AES encryption algorithm that uses string-based passwords to generate actual key
    /// </summary>
    public class PasswordAes : AdvancedAes
    {
        private static int _rfc2898DeriveBytesIterations = 10000;

        /// <summary>
        ///     Initializes a new instance of <see cref="PasswordAes" />
        /// </summary>
        /// <param name="password">password to generate key from</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="password" /> is null</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="password" /> is empty</exception>
        /// <remarks>
        ///     Salt remains the same unless <see cref="ResetSalt" /> is called. You have to reset salt when reusing the same
        ///     instance for encryption!
        /// </remarks>
        public PasswordAes(string password)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (password.Length == 0) throw new ArgumentException("Password cannot be empty", nameof(password));

            Password = password;
            Key = GenerateKey(Password, Salt);
        }

        /// <summary>
        ///     Number of iterations <see cref="Rfc2898DeriveBytes" /> does while creating a key from password.
        /// </summary>
        /// <remarks>
        ///     At least 1 000 iterations are advised, default value is 10 000
        /// </remarks>
        public static int Rfc2898DeriveBytesIterations
        {
            get { return _rfc2898DeriveBytesIterations; }
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(nameof(value), "The value must be grater than 0");

                _rfc2898DeriveBytesIterations = value;
            }
        }

        /// <summary>
        ///     Gets the password in use
        /// </summary>
        /// <remarks>
        ///     <see cref="SecureString" /> is not used because <see cref="Rfc2898DeriveBytes" /> doesn't support it
        /// </remarks>
        public string Password { get; }

        /// <summary>
        ///     Generates an encryption key from <paramref name="password" /> of any length. Key is 256 bits long.
        /// </summary>
        /// <param name="password">string- based password</param>
        /// <param name="salt">Salt to add</param>
        /// <returns>Key of fixed size 256bit</returns>
        protected static byte[] GenerateKey(string password, byte[] salt)
        {
            const int keyLength = SimpleAes.DefaultKeySize/8; //we use 256-bit AES = 32 bytes

            using (var alg = new Rfc2898DeriveBytes(password, salt))
            {
                alg.IterationCount = Rfc2898DeriveBytesIterations;
                return alg.GetBytes(keyLength);
            }
        }

        protected override byte[] GenerateDecryptionKey(byte[] salt)
        {
            return GenerateKey(Password, salt);
        }

        /// <inheritdoc />
        public override void ResetSalt()
        {
            base.ResetSalt();
            Key = GenerateKey(Password, Salt);
        }
    }
}